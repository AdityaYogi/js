const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
    
  res.send('Hello World!')
})
app.get('/students', (req, res) => {
    let students=[{
        name:"Pratik kumar",
        roll:"8"
    },
    {
        name:"Aditya kumar",
        roll:"10"
    },
]
    res.send(students)
  })
  

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})