
const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})
app.get('/members',(req ,res) => {
    let members= [ 
        {name:"aadi",mobile:"9097984258",age:"25"},
        {name:"shahnwaj",mobile:"8768765554",age:"75"} ]
     res.send(members)
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
